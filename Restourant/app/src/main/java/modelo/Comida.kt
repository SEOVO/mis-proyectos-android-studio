package modelo
import java.io.Serializable

open class Comida:Serializable {
    var nombre:String=""
    var precio:Double=0.0
    var imagen:Int=0
    constructor(){}
    constructor(nombre: String, precio: Double, imagen: Int) {
        this.nombre = nombre
        this.precio = precio
        this.imagen = imagen
    }

}