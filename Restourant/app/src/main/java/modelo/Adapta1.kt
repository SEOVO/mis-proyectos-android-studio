package modelo

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.restourant.R

class Adapta1(private val mlist:List<Comida>,private val itemClick:AViewHolder.onComidaOnclick):RecyclerView.Adapter<Adapta1.AViewHolder>() {
    class AViewHolder(item: View):RecyclerView.ViewHolder(item) {
        interface onComidaOnclick{
            fun onItemClick(comi: Comida)
        }

        //crear variables de programa
        val img1 = item.findViewById<ImageView>(R.id.imagen1)
        val desp = item.findViewById<TextView>(R.id.txt_descripcion)
        val precio = item.findViewById<TextView>(R.id.txt_precio)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AViewHolder {
        val vista=LayoutInflater.from(parent.context).inflate(R.layout.vista1,parent,false)
        return AViewHolder(vista)
    }

    override fun onBindViewHolder(holder: AViewHolder, position: Int) {
        var comi:Comida =mlist.get(position)
        holder.desp.setText(comi.nombre)
        holder.precio.setText(" "+comi.precio)
        holder.img1.setImageResource(comi.imagen)
        holder.itemView.setOnClickListener{
            itemClick.onItemClick(comi)
        }

    }

    override fun getItemCount(): Int {
        return mlist.size
    }

}