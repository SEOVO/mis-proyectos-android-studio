package com.example.restourant

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
//import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main_mariscos.*
import modelo.Adapta1
import modelo.Comida

class MainActivityMariscos : AppCompatActivity(), Adapta1.AViewHolder.onComidaOnclick {
    val lista:ArrayList<Comida> = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_mariscos)
        reciclerx.layoutManager = LinearLayoutManager(this)
        reciclerx.addItemDecoration(DividerItemDecoration(this,DividerItemDecoration.VERTICAL))
        llena()
        val adp = Adapta1(lista,this)
        reciclerx.adapter=adp


    }

    fun llena(){
        lista.add(Comida("Aji de Gallina",100.0,R.drawable.ajigallina))
        lista.add(Comida("Arroz Mar",200.0,R.drawable.arrozmar))
        lista.add(Comida("Arroz con Pollo",300.0,R.drawable.arrozpollo))
        lista.add(Comida("Camarones",400.0,R.drawable.camarones))
        lista.add(Comida("Causa",500.0,R.drawable.causa))
        lista.add(Comida("Choros",600.0,R.drawable.choros))

    }

    override fun onItemClick(comi: Comida) {
        var it:Intent= Intent(this,RecibeActivity::class.java)
        it.putExtra("dato",comi)
        startActivity(it)


    }
}