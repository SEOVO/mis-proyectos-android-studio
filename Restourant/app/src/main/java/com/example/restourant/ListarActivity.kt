package com.example.restourant

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_listar.*

class ListarActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_listar)
        var lista = myGlobal.lisart
        var cad:String="Productos Comprados \n"
        for (k in lista) {
            cad += "\nNombre :"+k.nombre+" precio: "+k.precio+" unidades: "+k.unidades
        }
        txt_lista.text = cad
    }
}