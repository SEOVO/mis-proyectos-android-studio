package com.example.restourant

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_recibe.*
import modelo.Comida
import modelo.Venta

class RecibeActivity : AppCompatActivity() {
    var comi = Comida()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recibe)
        comi = intent.getSerializableExtra("dato") as Comida
        img_recibe.setImageResource(comi.imagen)
        txt_recibe.setText("Nombre "+comi.nombre+" \n Precio"+comi.precio)

    }

    public fun retorna(v:View) {
        var it:Intent = Intent(this,MainActivityMariscos::class.java)
        startActivity(it)
    }

    public fun proceso(v:View) {
        var lista:ArrayList<Venta>
        if(myGlobal.lisart==null){
            lista=ArrayList<Venta>()
        }else{
            lista=myGlobal.lisart
        }
        var v = Venta()
        v.nombre = comi.nombre; v.precio = comi.precio ; v.imagen = comi.imagen
        v.unidades = txt_buy.text.toString().toInt()
        lista.add(v)
        Toast.makeText(applicationContext,"Dato Adicionado",Toast.LENGTH_LONG).show()
    }

    public fun muestra(v:View) {
        var it:Intent = Intent(this,ListarActivity::class.java)
        startActivity(it)
    }
}