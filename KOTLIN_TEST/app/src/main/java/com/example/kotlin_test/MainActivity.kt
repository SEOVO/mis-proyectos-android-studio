package com.example.kotlin_test

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import kotlin.math.*
import java.util.Random

class MainActivity : AppCompatActivity() , View.OnClickListener{
    lateinit var vara:EditText
    lateinit var varb:EditText
    lateinit var varc:EditText
    lateinit var ress:TextView
    lateinit var btnn:Button
    lateinit var varimg:ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        vara = findViewById(R.id.var_a)
        varb = findViewById(R.id.var_b)
        varc = findViewById(R.id.var_c)
        btnn = findViewById(R.id.btn)
        ress = findViewById(R.id.var_res)
        varimg = findViewById(R.id.res_img)
        //funcion boton
        btnn.setOnClickListener(this)

    }

    public fun onClick_old(v: View?) {
        var nombre = vara.text.toString();
        //consideramos que el 80% vale el final
        //y 20% el parcial
        var estatura = varb.text.toString().toDouble();
        var peso = varc.text.toString().toDouble();

        var imc =  (peso)/(estatura*estatura)

        var show_text = "Hola  "+nombre
        show_text += "\n tu imc es  : "+ imc.toString()


        //para la imagen
        var vec:Array<Int> = arrayOf(R.drawable.feliz,R.drawable.triste)

        //condicones del imc
        if (imc <= 18 ) {
            show_text += "\n Eres Delgado , ya que tu imc es menor o igual que 18  : "
            varimg.setImageResource(vec[0])
            ress.setTextColor(Color.parseColor("#1714f0"))
        }

        if (imc > 18 && imc <= 24   ) {
            show_text += "\n Eres Normal , ya que tu imc esta entre 18-24  : "
            varimg.setImageResource(vec[0])
            ress.setTextColor(Color.parseColor("#1714f0"))
        }

        if (imc > 24 && imc <= 28  ) {
            show_text += "\n Estas con sobrepeso , ya que tu imc esta entre 24-28  : "
            varimg.setImageResource(vec[1])
            ress.setTextColor(Color.parseColor("#f30825"))
        }

        if (imc > 28 ) {
            show_text += "\n Estas con  Obesidad, ya que tu imc es mayor a 28 : "
            varimg.setImageResource(vec[1])
            ress.setTextColor(Color.parseColor("#f30825"))
        }

        ress.setText(show_text)
    }

    fun cal_area(a:Int,b:Int,c:Int):String {
        var ax = a.toFloat()
        var bx = b.toFloat()
        var cx = c.toFloat()
        var sp=(ax+bx+cx)/2
        //sp = sp.toFloat()
        var spx = sp * (sp-ax)*(sp-bx)*(sp-cx)
        var area = sqrt(spx).toString()

        //var area = sp
        return area
    }

    fun verify_triangle(a:Int,b:Int,c:Int):String {

        var res = "Los numeros: "+a.toString()+" ,"+b.toString()+" , "+c.toString()

        if ((a +b) > c && (a+c) > b && (b+c) > a )  {
            res += "-> Si es un triangulo y su area es "+cal_area(a,b,c)
        }else {
            res += "-> No es un triangulo"

        }
        return res
    }
    override fun onClick(v: View?) {
        var a = vara.text.toString();
        //consideramos que el 80% vale el final
        //y 20% el parcial
        var b = varb.text.toString().toDouble();
        var c = varc.text.toString().toDouble();



        var show_text = verify_triangle(a.toInt(),b.toInt(),c.toInt())


        ress.setText(show_text)
    }
}