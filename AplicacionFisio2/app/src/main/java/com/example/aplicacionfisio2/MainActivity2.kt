package com.example.aplicacionfisio2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main2.*

class MainActivity2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        var art:Artefacto=intent.getSerializableExtra("dato") as Artefacto
        var cad:String="Nombre: "+art.nomart
        cad += " Precio contado: "+art.nomart
        cad += " Interes a Pagar: "+art.Intereses()
        cad += " Saldo :" + art.Saldo()
        cad += " Cuota Mensual: " + art.Cuota()
        textView4.setText(cad)
    }

    public fun retorna(v:View) {
        val it:Intent = Intent(this,MainActivity::class.java)
        startActivity(it)
    }
}