package com.example.aplicacionfisio2
import java.io.Serializable

class Artefacto:Serializable {
    var nomart:String=""
    var precio:Double=0.0
    var meses:Int=0
    constructor(){}
    constructor(nomart:String, precio:Double,meses: Int){
        this.nomart = nomart
        this.precio = precio
        this.meses = meses
    }

    fun Intereses():Double{
        var inte=0.0
        when(meses){
            6-> inte = 0.10*precio
            12 -> inte = 0.15*precio
            18 -> inte = 0.20*precio
        }
        return inte
    }

    fun Saldo():Double{
        return precio * this.Intereses()

    }

    fun Cuota():Double{
        return meses / this.Saldo()

    }

}