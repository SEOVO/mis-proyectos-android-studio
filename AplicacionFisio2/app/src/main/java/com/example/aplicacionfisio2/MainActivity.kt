package com.example.aplicacionfisio2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    var arte = Artefacto()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        spinner1.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                arte.nomart=spinner1.selectedItem.toString()
                val dire:Int=resources.getIdentifier("drawable/"+arte.nomart,null,packageName)
                imageView.setImageResource(dire)
                val precio:Array<Double> = arrayOf(560.0,1200.0,450.0,340.0,200.0,2600.0)
                textView2.setText("precio CONTADO "+precio[position])
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }
    }

    public fun calculo(v:View){
        when(rg1.checkedRadioButtonId) {
            R.id.radio1 -> arte.meses = 6
            R.id.radio2 -> arte.meses = 12
            R.id.radio3 -> arte.meses = 18

        }
    }
}