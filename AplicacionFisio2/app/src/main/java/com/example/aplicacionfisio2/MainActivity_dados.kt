package com.example.aplicacionfisio2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View

import kotlinx.android.synthetic.main.activity_main_dados.*
import java.util.Random

class MainActivity_dados : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_dados)
    }

    fun genera(inicio:Int , fin:Int):Int  {
        var rand = Random()
        return rand.nextInt(fin-inicio+1)+inicio
    }

    public fun lanzar(v:View) {

        var dir:Array<Int> = arrayOf(0,R.drawable.dado1,R.drawable.dado2,R.drawable.dado3,
            R.drawable.dado4,R.drawable.dado5,R.drawable.dado6
        )

        val d1 = genera(1,6)
        val d2 = genera(1,6)
        val sm = d1 + d2
        var cad:String=""
        if(sm==7 || sm==12)
            cad = "Gano !!!!"+sm
        else
            cad = "Intente de nuevo"+sm

        img1.setImageResource(dir[d1])
        img2.setImageResource(dir[d2])
        dados_res.setText(cad)

    }
}