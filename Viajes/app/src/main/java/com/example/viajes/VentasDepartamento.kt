package com.example.viajes

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import kotlinx.android.synthetic.main.activity_ventas_departamento.*

class VentasDepartamento : AppCompatActivity() {
    var depa = Departamento()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ventas_departamento)
        spinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                var area = spinner.selectedItem.toString().toInt()
                //depa.area = area
                var dire:Int = 0
                if (area == 70) {
                    dire = R.drawable.mini_departamento
                    depa.costo = 800000.0


                }
                if (area == 90) {
                    dire = R.drawable.depa
                    depa.costo = 1000000.0
                }

                if (area == 110) {
                    dire = R.drawable.depa_grande
                    depa.costo = 1500000.0
                }
                imageView2.setImageResource(dire)
                depa.img = dire.toString()
                depa.area = area

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }
        }
    }

    public fun calculos(v: View) {
        depa.cliente = cliente.text.toString()
        depa.years = a_pagar.text.toString().toInt()

        var itx: Intent = Intent(this,MainActivity2::class.java)
        itx.putExtra("depa",depa)
        startActivity(itx)
    }
}