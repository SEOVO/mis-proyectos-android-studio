package com.example.viajes

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View

import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    var viaje = Viaje()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    }

    public fun calculo(v: View) {
        var numpersonas = (editTextNumber1.text).toString().toInt()
        var numdias = (editTextNumber2.text).toString().toInt()
        when(radiogrupo.checkedRadioButtonId) {
            R.id.rbarequipa -> viaje = Viaje("Arequipa", numpersonas ,numdias , 130.0, R.drawable.cajamarca)
            R.id.rbcuzco -> viaje = Viaje("Cuzco", numpersonas ,numdias , 150.0,R.drawable.cuzco)
            R.id.rbtumbes -> viaje = Viaje("Tumbes", numpersonas ,numdias , 100.0, R.drawable.tumbes)
            R.id.rbcajamarca -> viaje = Viaje("Cajamarca", numpersonas ,numdias , 100.0, R.drawable.cajamarca)
        }

        var it:Intent = Intent(this,MainActivity2::class.java)
        it.putExtra("dato",viaje)

        startActivity(it)
    }
}