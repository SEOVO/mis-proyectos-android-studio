package com.example.viajes

import java.io.Serializable

class Departamento : Serializable {

    var cliente:String = ""
    var area:Int = 0
    var years:Int = 0
    var costo:Double = 0.0
    var img:String = ""
    constructor(){}
    constructor(cliente: String, area: Int, years: Int, costo: Double, img: String) {
        this.cliente = cliente
        this.area = area
        this.years = years
        this.costo = costo
        this.img = img
    }

    fun interes():Double {
        var inte = 0.0
        if (this.years <= 5) {
            inte = 10.0
        }

        if ( 5 < this.years && this.years  <= 9) {
            inte = 15.0
        }

        if ( 9 < this.years && this.years  <= 15) {
            inte = 25.0
        }

        if (this.years > 15) {
            inte = 28.0
        }

        return inte

    }

    fun cal_cuota():Double {

        var interes = interes()
        var i = ( interes / ( this.years * 12 ) ) / 100
        var n = this.years * 12.0
        var p =  Math.pow (1 + i ,  n )
        var cuota = this.costo *  (( p * i ) /  (p -1))
        return cuota

    }

    fun cal_saldo():Double {
        return this.costo - cal_cuota()

    }







    }