package com.example.viajes

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main2.*

class MainActivity2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        var depa:Departamento = intent.getSerializableExtra("depa") as Departamento
        if (depa.years >0 ){

            var cad:String = " Nombre del cliente: "+ depa.cliente
            cad += "\n  Tipo de Departamento: "+depa.area.toString() +" Mtrs"
            cad += "\n  Costo: "+depa.costo.toString()
            cad += "\n  Numero de Años: "+depa.years.toString()
            cad += "\n \n  Interes a Pagar:   "+depa.interes().toString()+" %  "
            cad += "\n  Cuota :  "+depa.cal_cuota().toString()
            cad += "\n  Saldo:  "+depa.cal_saldo().toString()
            msg_res.setText(cad)
            imageView.setImageResource(depa.img.toInt())

        }


        /*
        var viaj:Viaje = intent.getSerializableExtra("dato") as Viaje
        if (viaj.numpersonas >0 ){

            var cad:String = "Costo x Dia: "+viaj.precio_unit.toString()
            cad += "\n  Costo Total: "+viaj.amount_base().toString()
            cad += "\n  Descuento: "+viaj.discount().toString()
            cad += "\n  Total a Pagar: "+viaj.amount_total().toString()
            msg_res.setText(cad)
            imageView.setImageResource(viaj.img.toInt())

        }





         */


    }

    public fun retorna(v: View) {
        var it:Intent = Intent(this,MainActivity::class.java)
        startActivity(it)

    }
}