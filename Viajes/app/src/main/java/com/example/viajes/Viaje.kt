package com.example.viajes
import java.io.Serializable

class Viaje : Serializable {
    var name:String = ""
    var numpersonas:Int = 0
    var numdias:Int = 0
    var precio_unit:Double = 0.0
    var img:String = ""

    constructor(){}

    constructor(name: String, numpersonas: Int, numdias: Int, precio_unit: Double , img:Int) {
        this.name = name
        this.numpersonas = numpersonas
        this.numdias = numdias
        this.precio_unit = precio_unit
        this.img = img.toString()
    }

    fun discount():Double {
        var des = 0.0
        if (this.numpersonas > 4) {
            des = (this.precio_unit * this.numpersonas * this.numdias) * 0.15
        }
        return des
    }

    fun amount_base():Double {
        return this.precio_unit * this.numpersonas *  this.numdias * 1.0
    }

    fun amount_total():Double {
        return  this.amount_base() - this.discount()
    }
}