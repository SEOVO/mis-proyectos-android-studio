package com.example.appitgroup.ui.slideshow

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import androidx.core.content.contentValuesOf

class dataHelperPrestamo (context: Context): SQLiteOpenHelper(context, DATABASE_NAME,null, VERSION) {

    companion object{
        private val DATABASE_NAME="basepresta"
        private val VERSION=1
        private val TABLA_NAME="Prestamo"

    }

    val tabla="create table Prestamo(id integer primary key autoincrement,client text, monto real, mes integer)"


    override fun onCreate(db: SQLiteDatabase?) {
        //if(db!=null)
        db?.execSQL(tabla)


    }

    override fun onUpgrade(db: SQLiteDatabase?, p1: Int, p2: Int) {
        Log.w("Cambia","actualizando antigua"+p1+"con nueva"+p2)
        db?.execSQL("drop table if exists Prestamo")
        onCreate(db)
        db?.execSQL(tabla)

    }

    //los metodos de usuario
    fun adicion(p:Personas):Long{
        var db=this.writableDatabase // en la bdd
        var ct= contentValuesOf()
        ct.put("client",p.persona)
        ct.put("monto",p.email)
        ct.put("mes",p.imagen)
        val resp=db.insert(TABLA_NAME,null,ct)
        return resp;
    }

    fun cambia(p:Personas): Int{
        var db=this.writableDatabase // en la bdd
        var ct= contentValuesOf()
        //val condicion=Array<String> = arrayOf<String>(p.nropre.toString())
        ct.put("client",p.persona)
        ct.put("monto",p.email)
        ct.put("mes",p.imagen)
        // val resp=db.update(TABLA_NAME,ct,"id=?",condicion)
        val resp=db.update(TABLA_NAME,ct,"id="+p.nropre, null)
        return resp;
    }

    fun anula(id:Int):Int{
        val db=this.writableDatabase //Escribir en el bbd
        val resp=db.delete("Prestamo","id="+id,null)
        return resp
    }

    fun lisPres():List<Personas>{
        val lista:ArrayList<Personas> = ArrayList()
        val sql="Select * from Prestamo"
        var db=this.readableDatabase
        var cur=db.rawQuery(sql, null)
        while (cur.moveToNext()){
            var p=Personas()
            p.nropre=cur.getInt(0)
            p.persona=cur.getString(1)
            p.email=cur.getString(2)
            p.imagen=cur.getInt(3)
            lista.add(p)
        }
        return lista;
    }

}