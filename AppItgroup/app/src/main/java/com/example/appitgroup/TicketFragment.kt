package com.example.appitgroup

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.appitgroup.ui.slideshow.Personas
import com.example.appitgroup.ui.slideshow.adapta1
import com.example.appitgroup.ui.slideshow.dataHelperPrestamo
import kotlinx.android.synthetic.main.fragment_ticket.*


class TicketFragment: Fragment(), adapta1.Viewholder.onArtOnClick {
    lateinit var recy1:RecyclerView

    var lista: ArrayList<Personas> =ArrayList()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var v:View= inflater.inflate(R.layout.fragment_ticket, container, false)



        recy1=v.findViewById(R.id.recy1)
        recy1.layoutManager=LinearLayoutManager(requireContext())
        llena()
        val adp=adapta1(lista,this)
        recy1.adapter=adp

        return v

    }
    fun llena(){
        lista.add(Personas("ALEX",0,"alex34@gmail.com",R.drawable.avatar1))
        lista.add(Personas("STUART",0,"alex34@gmail.com",R.drawable.avatar2))
        lista.add(Personas("LORENZO",0,"alex34@gmail.com",R.drawable.avatar3))
        lista.add(Personas("GABRIELA",0,"alex34@gmail.com",R.drawable.avatar4))
        lista.add(Personas("JOE",0,"alex34@gmail.com",R.drawable.avatar5))
        lista.add(Personas("ALEXANDER",0,"alex34@gmail.com",R.drawable.avatar1))
        lista.add(Personas("MARIANO",0,"alex34@gmail.com",R.drawable.avatar2))
        lista.add(Personas("LUIS",0,"alex34@gmail.com",R.drawable.avatar3))
        lista.add(Personas("MICHELLE",0,"alex34@gmail.com",R.drawable.avatar4))
        lista.add(Personas("JUAN",0,"alex34@gmail.com",R.drawable.avatar5))

    }

    override fun onItemClick(personas: Personas) {
        TODO("Not yet implemented")
    }


}