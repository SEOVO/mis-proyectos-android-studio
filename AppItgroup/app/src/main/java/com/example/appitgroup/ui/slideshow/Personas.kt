package com.example.appitgroup.ui.slideshow

import java.io.Serializable

open class Personas:Serializable {
    var persona: String=""
    var nropre:Int=0
    var email: String=""
    var imagen:Int=0
    constructor()
    constructor(persona: String,nropre:Int, email: String, imagen: Int) {
        this.persona = persona
        this.nropre= nropre
        this.email = email
        this.imagen = imagen
    }

}