package clases

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import clases.AdaptaCurso.*
import com.example.appitgroup.R
import kotlinx.android.synthetic.main.vista_cursos.*
import kotlinx.android.synthetic.main.vista_cursos.view.*

class AdaptaCurso(private val mlist:List<Cursos>):RecyclerView.Adapter<ViewHolder>() {
    class ViewHolder(item: View):RecyclerView.ViewHolder(item) {
        //crear variables de programa para cada control de la vista
        //val img1 = item.findViewById<ImageView>(R.id.curso_icono)
        //val img1 = item.curso_icono
        //val name = item.txt_name
        //val descripcion = item.txt_descripcion

        val img1=item.findViewById<ImageView>(R.id.curso_icono)
        val name=item.findViewById<TextView>(R.id.txt_name)
        val descripcion =item.findViewById<TextView>(R.id.txt_descripcion)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val vista = LayoutInflater.from(parent.context).inflate(R.layout.vista_cursos,parent,false)
        return ViewHolder(vista)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var curs:Cursos = mlist.get(position)
        holder.descripcion.setText(curs.name)
    }

    override fun getItemCount(): Int {
        return mlist.size
    }


}