package clases
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.appitgroup.R

public class adapta1 (private val mlis:List<Personas>, private val itemClick: Viewholder.onArtOnClick):RecyclerView.Adapter<adapta1.Viewholder>() {

    class Viewholder(item:View):RecyclerView.ViewHolder(item){

        interface onArtOnClick{
            fun onItemClick(personas: Personas);
        }


        val img1=item.findViewById<ImageView>(R.id.imagenTicket)
        val email=item.findViewById<TextView>(R.id.emailTicket)
        val nombre=item.findViewById<TextView>(R.id.nombreTickey)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Viewholder {
       val vista=LayoutInflater.from(parent.context).inflate(R.layout.vistamenu,parent,false)
        return Viewholder(vista)
    }

    override fun onBindViewHolder(holder: Viewholder, position: Int) {
        var imagen: Personas =mlis.get(position)
        holder.img1.setImageResource(imagen.imagen)
        holder.email.setText(imagen.email)
        holder.nombre.setText(imagen.persona)

        holder.itemView.setOnClickListener(){
itemClick.onItemClick(Personas())
        }
    }

    override fun getItemCount(): Int {
       return mlis.size
    }
}