package com.example.fragment2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_2.*

class MainActivity : AppCompatActivity() , Fragment2.ComunicadorFragments {

    var numFragment = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button.setOnClickListener {
            val bundle = Bundle()
            bundle.putInt("numero",++numFragment)
            val transaction = supportFragmentManager.beginTransaction()
            val fragmento = Fragment1()
            fragmento.arguments = bundle
            transaction.replace(R.id.contenedor,fragmento)
            transaction.addToBackStack(null)
            transaction.commit()
        }

        button2.setOnClickListener {
            val transaction = supportFragmentManager.beginTransaction()
            val fragmento = Fragment2()
            transaction.replace(R.id.contenedor,fragmento)
            transaction.addToBackStack(null)
            transaction.commit()
        }

        button3.setOnClickListener {
            val transaction = supportFragmentManager.beginTransaction()
            val fragmento = Fragment3()
            transaction.replace(R.id.contenedor,fragmento)
            transaction.addToBackStack(null)
            transaction.commit()
        }
    }

    override fun devolverDato(dato: String) {
        txt_main.setText(dato)
    }
}