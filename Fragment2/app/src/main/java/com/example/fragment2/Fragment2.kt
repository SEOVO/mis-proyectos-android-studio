package com.example.fragment2

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import java.lang.RuntimeException


class Fragment2 : Fragment() {

    interface ComunicadorFragments {
        fun devolverDato(dato: String)
    }

    private var activityContenedora: ComunicadorFragments? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is ComunicadorFragments)
            activityContenedora = context
        else throw RuntimeException(
            context.toString()+" debe implementar comunicador fragments"
        )

    }

    override fun onDetach() {
        super.onDetach()
        activityContenedora = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_2, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val boton : Button = requireView().findViewById(R.id.btn2)
        boton.setOnClickListener {
            val et : EditText = requireView().findViewById(R.id.txt2)
            activityContenedora?.devolverDato(et.text.toString())
        }
    }
}