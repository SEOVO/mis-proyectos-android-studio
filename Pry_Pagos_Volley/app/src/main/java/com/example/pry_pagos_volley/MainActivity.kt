package com.example.pry_pagos_volley

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_main.*
import com.example.pry_pagos_volley.modelo.Adapta1
import com.example.pry_pagos_volley.modelo.Alu_Nota

import org.json.JSONException

class MainActivity : AppCompatActivity(), Adapta1.onAlumOnclick {
    //indicamos la url de conexion
    val url="https://morenouni26.000webhostapp.com/Controla.php"
    //indicamos la variable voley
    var voley : RequestQueue?=null
    //funcion oncreate el activity
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        voley = Volley.newRequestQueue(this)

    }

    //funcion filtar

    public fun filtrar(v: View){
        //obtener el campo a filtrar
        var ape:String = txt_filtro.text.toString()
        //concatenamos este filtro
        val direc=url+"?tag=filtra&ape="+ape
        //ejecutamos la consulta web
        val req= JsonObjectRequest(Request.Method.GET, direc,null, Response.Listener { response ->
            try {
                //obtenemos
                val vector= response.getJSONArray("dato")
                val lista: ArrayList<Alu_Nota> = ArrayList()
                for (f in 0 until vector.length()){
                    val fila =vector.getJSONObject(f)
                    var a = Alu_Nota()
                    a.coda=fila.getString("coda").toString()
                    a.ape=fila.getString("ape")
                    a.nom=fila.getString("nom")
                    lista.add(a)

                }
                val dp= Adapta1(lista,this)
                recy1.layoutManager= LinearLayoutManager(this)
                recy1.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
                recy1.adapter = dp
            }catch (ex: JSONException){
                ex.printStackTrace()
            }

        }, Response.ErrorListener{ error ->
            Toast.makeText(applicationContext,"error $error", Toast.LENGTH_SHORT).show()

        })
        voley?.add(req)
    }

    //

    override fun onBotonClick(a: Alu_Nota) {
        var it:Intent= Intent(this, ActivityNotas::class.java)
        it.putExtra("dato",a)
        startActivity(it)
    }

    override fun onBotonClick2(a: Alu_Nota) {
        var it:Intent= Intent(this, ActivityPagos::class.java)
        it.putExtra("dato",a)
        startActivity(it)
    }
}
