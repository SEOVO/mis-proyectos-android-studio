package com.example.pry_pagos_volley.modelo

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.pry_pagos_volley.R

//este adaptador va recibir la lista de artefactos y lo asiganar a los controles de la vista
class Adapta3(private val mlis:List<Alu_Pagos>):RecyclerView.Adapter<Adapta3.ViewHolder>() {

    class ViewHolder(item:View):RecyclerView.ViewHolder(item) {
      //para programar eventos en la seleccion de un item

        //crear variables de programa para cada control de la vista

        val textCurso=item.findViewById<TextView>(R.id.txtDetallePagos)


      }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val vista=LayoutInflater.from(parent.context).inflate(R.layout.vistapagos,parent,false)
        return ViewHolder(vista)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
      //relacionar las variables con el dato
        var a: Alu_Pagos =mlis.get(position)  //capturar el item de la lista
        holder.textCurso.setText("Ciclo : "+a.cic+"\nFecha de pago: "+a.fec+"\nMonto pagado: "+a.monto)

    }

    override fun getItemCount(): Int {
        return mlis.size
    }

}
