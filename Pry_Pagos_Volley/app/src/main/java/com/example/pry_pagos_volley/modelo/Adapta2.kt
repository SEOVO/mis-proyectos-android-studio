package com.example.pry_pagos_volley.modelo

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.pry_pagos_volley.R

class Adapta2(private val mlis:List<Alu_Curso>):RecyclerView.Adapter<Adapta2.ViewHolder>() {

    class ViewHolder(item:View):RecyclerView.ViewHolder(item) {
        val textCurso=item.findViewById<TextView>(R.id.txtDetallePagos)
      }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val vista=LayoutInflater.from(parent.context).inflate(R.layout.vistacursos,parent,false)
        return ViewHolder(vista)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
      //relacionar las variables con el dato
        var a: Alu_Curso =mlis.get(position)  //capturar el item de la lista
        holder.textCurso.setText("Codigo de curso: "+a.codc+"\nNombre del curso: "+a.nomc+"\nEvaluacion Parcial: "+a.ep+"\nEvaluacion Final: "+a.ef+"\nPromedio: "+a.prom())

    }
    override fun getItemCount(): Int {
        return mlis.size
    }
}
