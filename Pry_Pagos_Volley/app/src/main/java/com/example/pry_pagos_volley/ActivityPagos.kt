package com.example.pry_pagos_volley

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.bumptech.glide.Glide
import com.example.pry_pagos_volley.modelo.Adapta3
import com.example.pry_pagos_volley.modelo.Alu_Nota
import com.example.pry_pagos_volley.modelo.Alu_Pagos
import kotlinx.android.synthetic.main.activity_notas.*
import org.json.JSONException

class ActivityPagos : AppCompatActivity() {
    val url="https://morenouni26.000webhostapp.com/Controla.php"
    val url_img:String="https://morenouni26.000webhostapp.com/fotos/"
    var alum= Alu_Nota()
    var voley : RequestQueue?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pagos)
        voley = Volley.newRequestQueue(this)
        alum= intent.getSerializableExtra("dato") as Alu_Nota
        txtNombrePagos.setText(alum.ape+", "+alum.nom)
        muestraFoto(alum.coda)
        llena(alum.coda)
    }
    public fun Retornar (v: View){
        val it= Intent(this,MainActivity::class.java)
        startActivity(it)
    }
    fun muestraFoto(id:String){
        Glide.with(this)
            .load(url_img+id+".jpg")
            .error(R.drawable.sin_foto)
            .into(imgPicPagos)
    }
    fun llena(coda:String){
        val direc=url+"?tag=consulta2&coda="+coda
        val req= JsonObjectRequest(Request.Method.GET, direc,null, Response.Listener { response ->
            try {
                val vector= response.getJSONArray("dato")
                val lista: ArrayList<Alu_Pagos> = ArrayList()
                for (f in 0 until vector.length()){
                    val fila =vector.getJSONObject(f)
                    var a = Alu_Pagos()
                    a.cic=fila.getString("cic").toString()
                    a.fec=fila.getString("fec")
                    a.monto=fila.getString("mon").toInt()
                    lista.add(a)

                }
                val dp= Adapta3(lista)
                recy3.layoutManager= LinearLayoutManager(this)
                recy3.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
                recy3.adapter = dp
            }catch (ex: JSONException){
                ex.printStackTrace()
            }

        }, Response.ErrorListener{ error ->
            Toast.makeText(applicationContext,"error $error", Toast.LENGTH_SHORT).show()

        })
        voley?.add(req)
    }
}
