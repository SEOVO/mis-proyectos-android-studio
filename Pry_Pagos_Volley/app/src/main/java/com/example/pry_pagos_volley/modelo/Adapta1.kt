package com.example.pry_pagos_volley.modelo

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.pry_pagos_volley.R


class Adapta1(private val mlis:List<Alu_Nota>, private val itemClick:onAlumOnclick):RecyclerView.Adapter<Adapta1.ViewHolder>() {

    interface onAlumOnclick{
        fun onBotonClick(a: Alu_Nota)
        fun onBotonClick2(a: Alu_Nota)
    }
    class ViewHolder(item:View):RecyclerView.ViewHolder(item) {
        val cod=item.findViewById<TextView>(R.id.txtCodigo)
        val nombre=item.findViewById<TextView>(R.id.txtNombre)
        val ape=item.findViewById<TextView>(R.id.txtApellido)
        val btnNotas=item.findViewById<Button>(R.id.btnNotas)
        val btnPagos=item.findViewById<Button>(R.id.btnPagos)
      }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val vista=LayoutInflater.from(parent.context).inflate(R.layout.vista,parent,false)
        return ViewHolder(vista)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var a: Alu_Nota =mlis.get(position)  //capturar el item de la lista
        holder.cod.setText(a.coda)
        holder.nombre.setText(a.nom)
        holder.ape.setText(a.ape)
        holder.btnNotas.setOnClickListener{
            itemClick.onBotonClick(a)
        }
        holder.btnPagos.setOnClickListener{
            itemClick.onBotonClick2(a)
        }

    }

    override fun getItemCount(): Int {
        return mlis.size
    }

}
