package com.example.pry_pagos_volley

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.bumptech.glide.Glide
import com.example.pry_pagos_volley.modelo.Adapta2
import com.example.pry_pagos_volley.modelo.Alu_Curso
import com.example.pry_pagos_volley.modelo.Alu_Nota
import kotlinx.android.synthetic.main.activity_notas.*
import org.json.JSONException

class ActivityNotas : AppCompatActivity() {
    val url="https://morenouni26.000webhostapp.com/Controla.php"
    val url2:String="https://morenouni26.000webhostapp.com/fotos/"
    var alum= Alu_Nota()
    var cola : RequestQueue?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notas)
        cola = Volley.newRequestQueue(this)
        alum= intent.getSerializableExtra("dato") as Alu_Nota
        txtNombrePagos.setText(alum.ape+", "+alum.nom)
        muestraFoto(alum.coda)
        llena(alum.coda)
    }
    public fun Retornar (v:View){
        val it= Intent(this,MainActivity::class.java)
        startActivity(it)

    }
    fun muestraFoto(id:String){
        Glide.with(this)
            .load(url2+id+".jpg")
            .error(R.drawable.sin_foto)
            .into(imgPicPagos)
    }
    fun llena(coda:String){
        val direc=url+"?tag=consulta1&coda="+coda
        val req= JsonObjectRequest(Request.Method.GET, direc,null, Response.Listener { response ->
            try {
                val vector= response.getJSONArray("dato")
                val lista: ArrayList<Alu_Curso> = ArrayList()
                for (f in 0 until vector.length()){
                    val fila =vector.getJSONObject(f)
                    var a = Alu_Curso()
                    a.codc=fila.getString("codc").toString()
                    a.nomc=fila.getString("nomc")
                    a.ep=fila.getString("ep").toInt()
                    a.ef=fila.getString("ef").toInt()
                    lista.add(a)

                }
                val dp= Adapta2(lista)
                recy3.layoutManager= LinearLayoutManager(this)
                recy3.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
                recy3.adapter = dp
            }catch (ex: JSONException){
                ex.printStackTrace()
            }

        }, Response.ErrorListener{ error ->
            Toast.makeText(applicationContext,"error $error", Toast.LENGTH_SHORT).show()

        })
        cola?.add(req)
    }
}
