package com.example.ahorcado

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
    public fun play(v:View) {
        var alea = alea(1,60)
        var enter = editTextNumber.text.toString().toInt()
        if (enter.equals(alea)) {
            textView2.setText("GANASTES")
            intento.setText("0")
            imageView2.setImageResource(R.drawable.ic_launcher_foreground)
        }else  {
            var cont = intento.text.toString().toInt()
            cont += 1
            intento.text = cont.toString()
            textView2.text = "INTENTO "+cont.toString()+" El VALOR A "+alea.toString()
            var imx = R.drawable.h1
            when(cont) {
                1 -> imx = R.drawable.h1
                2 -> imx = R.drawable.h2
                3 -> imx = R.drawable.h3
                4 -> imx = R.drawable.h4
                5 -> imx = R.drawable.h5clea
                6 -> imx = R.drawable.h6
            }
            if (cont >= 6) {
                textView2.text = "PERDISTES INTENTE NUEVAMENYE"
                intento.text = "0"
                imageView2.setImageResource(R.drawable.ic_launcher_foreground)
            }
            imageView2.setImageResource(imx)

        }
    }

    fun alea(Inicio:Int,Fin:Int):Int {
        var rand = Random()

        return rand.nextInt(Fin-Inicio+1) + Inicio
    }
}